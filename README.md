# csv文字化けテスト

## before using
- `$ npm install`を実行し、パッケージをインストールする必要あり

## jconvによる変換テスト
- 実行は`$ node conv.js`
- utf.txt(utf-8のテキストファイル)を入力とし、sjis.txt(shift-jisのテキストファイル)を出力

## csvへのBOM(byte order mark)付加テスト
- 実行は`$ node addbom.js`
- nobom.csvを入力とし、withbom.csvを出力
 - エンコーディングは共にutf-8
 - withbom.csvはExcelでも化けないはず(バージョンによるとの情報もあり)