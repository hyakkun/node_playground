var fs = require('fs');
var jconv = require('jconv');

var inputStr = fs.readFileSync('utf.txt');

var convertedStr = jconv.convert(inputStr, 'UTF8', 'SJIS');

fs.writeFileSync('sjis.txt', convertedStr);
