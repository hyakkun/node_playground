var fs = require('fs');

var bom = new Buffer(3);
bom[0] = 0xEF;
bom[1] = 0xBB;
bom[2] = 0xBF;

var inBuf = fs.readFileSync('nobom.csv');

var outBuf = Buffer.concat([bom, inBuf], bom.length + inBuf.length);

fs.writeFileSync('withbom.csv', outBuf);
